# softposit

This is a python wrapper to SoftPosit. 

To install:

	python setup.py install

To install in user space

	python setup.py install --user

To force update:

	pip install --no-cache-dir softposit --user


